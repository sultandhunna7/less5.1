variable "web_instance_count" {
  type    = number
  default = 3
}

variable "remote_user" {
  description = "user for remote-exec provisioner"
  default = "root"
}

variable "remote_key" {
  description = "ssh key for remote-exec provisioner"
  default = "~/.ssh/id_rsa"
}